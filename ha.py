#!/bin/python
import os,sys

jobID=int(sys.argv[1])
cores = 16

print "jobID = " +str(jobID)

MY_MATLAB_FOLDER="/scratch2/jianwel/matlab-code"
if not os.path.exists(MY_MATLAB_FOLDER):
	os.system("cp -ra /home/jianwel/matlab-code /scratch2/jianwel/")

os.chdir(MY_MATLAB_FOLDER);
wifiC=[0.8, 0.6, 0.4, 0.2]

wifi = wifiC[(jobID-1)/2]
isboth= (jobID%2==0)
print("taskset -c 0-%d matlab -nodisplay -nosplash -r 'XIN_wifi=[%.1f];XIN_isboth=[%d];addErrorPa' > runAddError%d.log"%(cores-1, wifi, isboth, jobID))
os.system("taskset -c 0-%d matlab -nodisplay -nosplash -r 'XIN_wifi=[%.1f];XIN_isboth=[%d];addErrorPa' > runAddError%d.log"%(cores-1, wifi, isboth, jobID))
#taskset -c 0-$(($OMP_NUM_THREADS-1)) 
