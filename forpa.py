#!/usr/bin/python

import os
import sys
from ash import *
from ashShaders import *



CmdLineEntries = []
CmdLineDefault = []
CmdLineActual= []

trianglemeshList=[]
bvhList=[]
anyList=[]


def CmdLineFindIndex( tag ):
	for i in range(len(sys.argv)):
		if sys.argv[i] == tag:
			return i
	return -1

def CmdLineFind( tag, defaultvalue ):
	CmdLineEntries.append( tag )
	CmdLineDefault.append( defaultvalue )
	i = CmdLineFindIndex(tag)
	if i > 0:
		if i < len(sys.argv)-1:
			return sys.argv[i+1]
	return defaultvalue

def CmdLineFindTuple( tag, nbElements ):
	i = CmdLineFindIndex(tag)
	CmdLineEntries.append( tag )
	CmdLineDefault.append( str(nbElements) + " items" )
	defstring = ""
	result = []
	if i > 0:
		if i < len(sys.argv)- nbElements+1:
			for j in range(0,nbElements):
				result.append(sys.argv[i+1+j])
				defstring = defstring + str(sys.argv[i+1+j]) + " "
	CmdLineActual.append( defstring )
	return result


def CmdLineFindIndices( tag ):
	indices = []
	for i in range(len(sys.argv)):
		if sys.argv[i] == tag:
			indices.append(i)
	return indices

def CmdLineFindArray( tag, defaultvalue ):
	CmdLineEntries.append( tag )
	CmdLineDefault.append( defaultvalue )
	clfList = CmdLineFindIndices(tag)
	clfArray = []
	if i in clfList:
		if i < len(sys.argv)-1:
			clfArray.append( sys.argv[i+1] )
	return clfArray

def CmdLineFindTuples( tag, nbElements ):
	CmdLineEntries.append( tag )
	CmdLineDefault.append( str(nbElements) + " items" )
	idx = CmdLineFindIndices(tag)
	result = []
	for i in idx:
		if i < len(sys.argv) - nbElements+1:
			elem = []
			for j in range(0,nbElements):
				elem.append(sys.argv[i+1+j])
			result.append( elem )
	return result

def CmdLineHelp( tag ):
	i = CmdLineFindIndex(tag)
	if i > 0:
		if len(CmdLineEntries) > 0:
			nbchmax = 0
			for parm in CmdLineEntries:
				nbch = len(parm)
				if nbchmax < nbch:
					nbchmax = nbch
			print "Option \tDefault value"
			for j in range(0,len(CmdLineEntries) ):
				extra = ""
				nbextra = nbchmax - len(CmdLineEntries[j])
				for b in range(0,nbextra):
					extra = extra + " "
				print CmdLineEntries[j] + extra + " \t" + str( CmdLineDefault[j] )
		sys.exit()

def PrintParameters( choices ):
	nbchmax = 0
	for parm, value in choices.iteritems():
		nbch = len(parm)
		if nbchmax < nbch:
			nbchmax = nbch
	nbextra = nbchmax - len("Parameters")
	extra = "Parameters"
	for b in range(0,nbextra):
		extra = extra + " "
	print "============================================================================"
	print extra + " -------> Value"
	parameters = choices.keys()
	parameters.sort()
	for parm in parameters:
		value = choices[parm]
		nbch = len(parm)
		nbextra = nbchmax - nbch
		extra = ""
		for b in range(0,nbextra):
			extra = extra + " "
		print str(parm) + extra + " -------> " + str(value)
	print "============================================================================"




def CmdLineFindSet( choices ):
	result = {}
	for parm, value in choices.iteritems():
		result[parm] = value
		if parm.count("---")>0:
			newvalue = CmdLineFindTuple( parm, 3 )
			if len(newvalue) == 3:
				result[parm] = newvalue
		elif parm.count("--")>0:
			newvalue = CmdLineFindTuple( parm, 2 )
			if len(newvalue) == 2:
				result[parm] = newvalue
		elif parm.count("-") >0:
			newvalue = CmdLineFind( parm, value  )
			result[parm] = newvalue
		else:
			newvalue = CmdLineFindIndex( parm )
			if newvalue > 0:
				result[parm] = True
	return result


def CmdLineFindValues( choices ):
	result = {}
	for parm, value in choices.iteritems():
		result[parm] = value
		if isinstance( value, str ):
			newValue = CmdLineFind( parm, value )
			result[parm] = newValue
		else:
			try:
				if len(value) > 1:
					nbArgs = len(value)
					newvalue = CmdLineFindTuple( parm, nbArgs )
					if len(newvalue) == nbArgs:
						result[parm] = newvalue
			except:
				newValue = CmdLineFind( parm, value )
				result[parm] = newValue
	return result



def RenderScene( scn, cam, img, nbsam, nbhits, nbthreads, tilex, tiley ):
	LOG_INFO() << "Setting up raybundle.\n"
	rb = RayBundle()
	LOG_INFO() << "Setting up tracer\n"
	tracer = RayTracer(scn)
	tracer.setNumberOfThreads( nbthreads )
	tracer.setMaxTraceDepth( nbhits )
	raygen = TileSchemeARayGenerator(cam, img, tilex, tiley, nbsam)
	meter = ProgressMeter( raygen.nbTiles(), "Render" )
	while  raygen.getRayBundle(rb):
		output = RayBundle()
		tracer.trace(rb, output)
 		Sort( output, img )
 		while rb.nbRays() > 0:
			rb.popRay()
		meter.update()

def calcCenter(surf):
    n = surf.getNbVertices();
    sumCenter = Vector(0,0,0)
    for i in range(0, n):
        sumCenter += surf.getVertex(i)
    sumCenter /= n
    return sumCenter


def ScaleObject(surf, center, factor):
    n = surf.getNbVertices()
    for i in range(0, n):
        vertex = surf.getVertex(i)
        vToC = vertex - center
        vertex = vertex - vToC*(1-factor)
        surf.setVertex(i, vertex)
    return surf

def TranslateObject(surf, vec, lenth):
    n = surf.getNbVertices()
    for i in range(0, n):
        vertex = surf.getVertex(i)
        vertex = vertex + vec*lenth
        surf.setVertex(i, vertex)
    return surf

def Flip(vector):
    vector.set(-vector.X(), -vector.Y(), -vector.Z())
    return vector

def TranslateCube(surf, vec, lenth):
    n = 12
    for i in range(0, n):
        tri = surf.getTriangle(i)
        for j in range(0,3):
            vertex = tri.getVertex(i)
            vertex = vertex + vec*lenth
    return surf

def drawCube( hl, center):
    global anyList
    trim = TriangleMesh()
    anyList.append(trim)

    v1 = Vector(-hl, -hl, -hl)
    v2 = Vector(-hl, -hl, hl)
    v3 = Vector(hl, -hl, hl)
    v4 = Vector(hl, -hl, -hl)
    v5 = Vector(-hl, hl, -hl)
    v6 = Vector(-hl, hl, hl)
    v7 = Vector(hl, hl, hl)
    v8 = Vector(hl, hl, -hl)

    anyList.append(v1)
    anyList.append(v2)
    anyList.append(v3)
    anyList.append(v4)
    anyList.append(v5)
    anyList.append(v6)
    anyList.append(v7)
    anyList.append(v8)
#b
    tri = Triangle(v1, v2, v4)
    anyList.append(tri)
    trim.Add(tri);

    tri = Triangle(v2, v3, v4)
    anyList.append(tri)
    trim.Add(tri);
#top
    tri = Triangle(v5, v6, v8)
    anyList.append(tri)
    trim.Add(tri);

    tri = Triangle(v6, v7, v8)
    anyList.append(tri)
    trim.Add(tri);

#left
    tri = Triangle(v5, v6, v2)
    anyList.append(tri)
    trim.Add(tri);

    tri = Triangle(v5, v2, v1)
    anyList.append(tri)
    trim.Add(tri);

#right
    tri = Triangle(v8, v7, v3)
    anyList.append(tri)
    trim.Add(tri);

    tri = Triangle(v8, v3, v4)
    anyList.append(tri)
    trim.Add(tri);

#front
    tri = Triangle(v6, v2, v3)
    anyList.append(tri)
    trim.Add(tri);

    tri = Triangle(v6, v3, v7)
    anyList.append(tri)
    trim.Add(tri);

#back
    tri = Triangle(v5, v1, v4)
    anyList.append(tri)
    trim.Add(tri);

    tri = Triangle(v5, v4, v8)
    anyList.append(tri)
    trim.Add(tri);

    #ScaleObject(surf, Vector(0,0,0), factor)
    #TranslateCube(trim, center, 1.0)
    return trim;



print versionString()

fov = float(CmdLineFind("-fov", 80.0))



############
objFile = CmdLineFind("-objfile", "../models/ajax/ajax.obj")

objMesh = SurfaceFile()
anyList.append(objMesh)
objMesh.read(objFile)
triangleMesh = TriangulateSurface(objMesh)
trianglemeshList.append(triangleMesh)

# BEGIN GIANTHACK
#lcol = Color( 0.886, 0.14, 0.38, 0.0 )
lcol = Color( 0.5,0.5,0.5, 0.0 )
anyList.append(lcol)
lambert = Lambertian(lcol)
anyList.append(lambert)




fresnel = FresnelRays(1.7)
anyList.append(fresnel)
push = PushToOutputStack()
anyList.append(push)
noReflectionMat = Material()
#noReflectionMat.addShader(env)
anyList.append(noReflectionMat)
#noReflectionMat.addShader(lambert)
noReflectionMat.addShader(fresnel)
#noReflectionMat.addShader(fresnel)
noReflectionMat.addShader(push)
#noReflectionMat.addShader(phong)
#noReflectionMat.addShader(refl)

nbTris = triangleMesh.returnN()

for i in range(0, nbTris):
	triangleMesh.setTriangleMaterial(i, noReflectionMat)


bvh = Bvh(triangleMesh)
bvhList.append(bvh)
#bvh.append(triangleMesh)

#bvh = Bvh( triangleMesh )
#bvhList.append(bvh)
###############



objMesh1 = SurfaceFile()
anyList.append(objMesh1)
objMesh1.read(objFile)

center = calcCenter(objMesh1)
objMesh1 = ScaleObject(objMesh1, center, 0.3)
objMesh1 =TranslateObject(objMesh1, Vector(0,0.3, -0.1), 1)
triangleMesh = TriangulateSurface(objMesh1)
trianglemeshList.append(triangleMesh)

# BEGIN GIANTHACK
lcol = Color( 0.89, 0.34, 0.11, 0.0 )
#lcol = Color( 0.5,0.5,0.5, 0.0 )
anyList.append(lcol)
lambert = Lambertian(lcol)
anyList.append(lambert)




push = PushToOutputStack()
anyList.append(push)
noReflectionMat = Material()
anyList.append(noReflectionMat)
noReflectionMat.addShader(lambert)
noReflectionMat.addShader(push)
#noReflectionMat.addShader(phong)
#noReflectionMat.addShader(refl)

nbTris = triangleMesh.returnN()

for i in range(0, nbTris):
	triangleMesh.setTriangleMaterial(i, noReflectionMat)


bvh1 = Bvh(triangleMesh)
bvhList.append(bvh1)
#bvh1.append(triangleMesh)


objFile = CmdLineFind("-objfile", "../testsuite/exportedDataFromMaya/landscape_basicshapes.mb/0010/objects/pSphere14.obj")
objMeshCube= SurfaceFile()
anyList.append(objMeshCube)
objMeshCube.read(objFile)


centercube = calcCenter(objMeshCube)
centercube.set(-centercube.X(), -centercube.Y(), -centercube.Z())

#move to center
objMeshCube = TranslateObject(objMeshCube, centercube, 1)

ScaleObject(objMeshCube, Vector(0,0,0), 35)
cube = TriangulateSurface(objMeshCube)
trianglemeshList.append(cube)



anyList.append(cube)
#cube = drawCube(10, center)
#triangleCube= TriangulateSurface(cube)

cubeM = Material()
anyList.append(cubeM)
killP = KillPrimaryRay(1)
anyList.append(killP)
lowC = Color(0.3, 0.3, 0.3, 0.0)
highC = Color(0.9, 0.9, 0.9, 0.0)

#hcol = Color( 0.2, 0.14, 0.88, 1.0 )
#anyList.append(hcol)
#lowcol = hcol / 3.0
#anyList.append(lowcol)

env = EnvironmentMap(highC, lowC)
anyList.append(env)

#push = PushToOutputStack()
#anyList.append(push)


cubeM.addShader(env)
cubeM.addShader(killP)
#cubeM.addShader(killP)
cubeM.addShader(push)
nbTris = cube.returnN()

for i in range(0,nbTris):
	cube.setTriangleMaterial(i, cubeM)

bvhenv = Bvh(cube)
bvhList.append(bvhenv)
#bvh.append(cube)

#bvh.Build()
#bvh1.Build()


LOG_INFO() << "Setting up image.\n"
nx = int(CmdLineFind("-NX", 1920))
ny = int(CmdLineFind("-NY", 1080))
image = Image()
image.reset( nx, ny )

camera = Camera()
camera.setEyeViewUp(Vector( 0, 0, 3.3), Vector(0,0,-1), Vector(0,1,0))
camera.setFov( fov )

LOG_INFO() << "Setting up raybundle.\n"
nbsamples = int(CmdLineFind("-nbsamples", 10 ))
rb = RayBundle()

tile_width = int(CmdLineFind("-tilewidth", 64))
tile_height = int(CmdLineFind("-tileheight", 64))


LOG_INFO() << "Setting up scene\n"
scene  = NewScene()
scene.addObject(bvh)
scene.addObject(bvh1)
scene.addObject(bvhenv)
#light =  PointLight(Vector(1,10,1), Color(1,1,1,1), Light.NO_DECAY)
light =  PointLight(Vector(4,4,4), Color(1,1,1,1), Light.NO_DECAY)
scene.addLight( light )

nbhits = 50
#nbhits = 10000
nthreads = int(CmdLineFind( "-nthreads", 16 ))

RenderScene( scene, camera, image, nbsamples, nbhits, nthreads, tile_width, tile_height )

LOG_INFO() << "Write \"noReflection.exr\" file\n"
oiioname =  "noReflection.exr"
writeOIIOImage( oiioname, image )





