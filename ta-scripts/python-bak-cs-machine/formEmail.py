#!/usr/bin/python
import csv
import os
import time

def MakeDict(a):
    if(not os.path.exists(a)):
        os.makedirs(a);

hwNo = 7
classNumber = "604"
emailPath = classNumber+"Email"

MakeDict(emailPath);

with open('hw'+str(hwNo)+'-score-'+classNumber+'.csv', 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    #print spamreader[1]
    title = reader.next();
    #print title[2];
    #titleList = title[1,-1];
    os.chdir(emailPath);
    MakeDict("hw"+str(hwNo));
    os.chdir("hw"+str(hwNo));
    i =0;
    while(1):
        l = reader.next();
        #print l
        emailAdd = l[0]+"@clemson.edu";
        emailFileName = l[0]+"_score.txt";
        ef = open(emailFileName, "w");
        ef.write("Hi "+l[0]+",\n\n")
        #ef.write("Sorry, 404 students, the score I just sent out has some problem, please use this one as the correct one.\n\n")
        #ef.write("For the extra points part, due to Dr Levine's standard, you will get 1 extra point if you implemented the full 604 extension. You will get one extra point if you implemented some other extra interesting features which show us you have made substantially more efforts to implement them.\n\n")
        ef.write("Your score of lab"+ str(hwNo)+ " is "+l[1]+".\n\n")

        ef.write("**You may read the following two abbrevations in my comment. Test C1 means the test of the test of a counterclockwise rotation for 30 degree followed by a clockwise rotation for 30 degree. (r 30  r -30 d) Test C2 means the test of  a translation followed by a perspective warp. (t -50 -50  p 0.001 0.001 d)\n\n")
        ef.write("The breakdown of score:\n");
        k =0;
        i =i+1;
   #     scoreList =l[2:-1];
   #     titleList =title[2:-1];
        scoreList =l[2:];
        titleList =title[2:];

        #print scoreList
        #print titleList
        #for (s) in (scoreList):
        for (t) in (titleList):
            #jt = title[k];
            s = scoreList[k];
            if(t=="Basic Sum" or t=="Late fee" or  t =="Ext Sum" or t== "Extra Credit"):
                ef.write("\t\t\t\t\t");
            ef.write(t + ": "+ s +"\n");
            if(t=="comments"):
                ef.write("\n");
            k=k+1;

        ef.write("\n\n");
        ef.write("\t\t\t\t\t");
        ef.write("Final Score:" + l[1]);
        ef.write("\n\n");

#        ef.write("\nComments:\n"+l[len(l)-1])
#        ef.write("\n\n");
        ef.close();
        #emailAdd = "jianwel@clemson.edu"
       # emailAdd = "ljw725@gmail.com"
    #    os.system('mail -s "'+ classNumber+" score - lab"+str(hwNo)+'" '+emailAdd + " < "+emailFileName);
        print('mail -s "'+ classNumber+" score - lab"+str(hwNo)+'" '+emailAdd + " < "+emailFileName);
        time.sleep(1);
        #print emailAdd;
        #print l;
    #for row in spamreader:
    #   print ', '.join(row)
