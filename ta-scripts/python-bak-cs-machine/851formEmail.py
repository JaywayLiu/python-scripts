#!/usr/bin/python
import csv
import os, sys
import time

def MakeDict(a):
    if(not os.path.exists(a)):
        os.makedirs(a);

hwNo = sys.argv[1]
classNumber = "851"
emailPath = classNumber+"Email"
total = 50;

MakeDict(emailPath);

with open('hw'+str(hwNo)+'-score-'+classNumber+'.csv', 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    #print spamreader[1]
    title = reader.next();
    print title;
    print title[2];
    #titleList = title[1,-1];
    os.chdir(emailPath);
    MakeDict("hw"+str(hwNo));
    os.chdir("hw"+str(hwNo));
    i =0;
    while(1):
        l = reader.next();
        #print l
        emailAdd = l[2]+"@clemson.edu";
        emailFileName = l[2]+"_score.txt";
        ef = open(emailFileName, "w");
        ef.write("Hi "+l[1]+" ("+l[2]+"),\n\n")
#        ef.write("**Hi, everyone in 851, Since I was just informed that the calculation of mean deviation is a 2-point extra credit, I revised the grades. If you got 5 points off because of that, you will find you get it back. If you implemented that and printed it out correctly, you will find that you get a 2-point extra credit. If I messed up with the xls calculation, please let me know. **\n\n")
        #ef.write("For the extra points part, due to Dr Levine's standard, you will get 1 extra point if you implemented the full 604 extension. You will get one extra point if you implemented some other extra interesting features which show us you have made substantially more efforts to implement them.\n\n")

        ef.write("This is only the sore of the programming part. Your score of lab"+ str(hwNo)+ " is "+l[3]+". (Total: "+str(total)+")\n\n")
        ef.write("The breakdown of score:\n");
        k =0;
        i =i+1;
        scoreList =l[2:-1];
        titleList =title[2:-1];
        #print scoreList
        #print titleList
        #for (s) in (scoreList):
        for (t) in (titleList):
            #jt = title[k];
            s = scoreList[k];
            if(t=="Basic Sum" or t =="EXT" or t=="Final Score"):
                ef.write("\t\t\t\t\t");
            if(not (t=="mdev(extra credit, 2)" and s =="0")):
                ef.write(t + ": "+ s +"\n");
            k=k+1;

        ef.write("\n\n");
        ef.write("\t\t\t\t\t");
        ef.write("Final Score:" + l[3]);
        ef.write("\n\n");

        ef.write("\nComments:\n"+l[len(l)-1])
        ef.write("\n\n");


        ef.write("************************************************************************\n");
        ef.write("Please read these policies carefully, you will lose points next time if you do not conform to them\n\n");

        #ef.write("**Hi,there, \n contact me if your score is zero. **\n\n")
        ef.close();

        os.system("cat "+"/home/jianwel/Documents/851Notice.txt >> "+emailFileName);
        #emailAdd = "jianwel@clemson.edu"
       # emailAdd = "ljw725@gmail.com"
        #os.system('mail -s "'+ classNumber+" score - lab"+hwNo+'" '+emailAdd + " < emailFileName");
        os.system('mail -s "'+ classNumber+"updated score - lab"+str(hwNo)+'" '+emailAdd + " < "+emailFileName);
        print('mail -s "'+ classNumber+"updated score - lab"+str(hwNo)+'" '+emailAdd + " < "+emailFileName);
        time.sleep(1);
        #print emailAdd;
        #print l;
    #for row in spamreader:
    #   print ', '.join(row)
