import os;
import glob;
import commands;

projectName = "imgview"
if(glob.glob("*.tgz")):
    os.system("tar -zxvf *.tgz");
if(glob.glob("*.tar.gz")):
    os.system("tar -zxvf *.tar.gz");

os.system("make clean > clean.log");
os.system("make > make.log");

#(status, output) = commands.getstatusoutput("make > make.log");
#print os.getenv('$?');
#print "status: "+str(status);
#print "output: \n"+ output;

os.system("""echo "\n\n compilation result: \n" >> make.log """);
if(os.path.exists(projectName)):
    os.system("""echo "compile correctly." >> make.log """);
    os.system("""echo "1" > onebit.log """);
else:
    os.system("""echo "%s not generated" >> make.log """%projectName);
    os.system("""echo "0" > onebit.log """);
