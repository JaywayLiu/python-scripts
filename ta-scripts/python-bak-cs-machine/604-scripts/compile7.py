import os;
import glob;
import commands;


objList = ["warper"]
if(glob.glob("*.tgz")):
    os.system("tar -zxvf *.tgz");
if(glob.glob("*.tar.gz")):
    os.system("tar -zxvf *.tar.gz");
if(glob.glob("*.tar")):
    os.system("tar -xvf *.tar.gz");

if(os.path.exists("CMakeLists.txt")):
        os.system("cmake .")

os.system("make clean > clean.log");
os.system("make > make.log");

#(status, output) = commands.getstatusoutput("make > make.log");
#print os.getenv('$?');
#print "status: "+str(status);
#print "output: \n"+ output;

os.system("""echo "\n\n compilation result: \n" >> make.log """);

if(os.path.exists("onebit.log")):
        os.system("rm -f onebit.log");

for obj in objList:
    if(os.path.exists(obj)):
        os.system("""echo "compile %s correctly." >> make.log """%obj);
        os.system("""echo "%s 1" >> onebit.log """%obj);
    else:
        os.system("""echo "%s not generated" >> make.log """%obj);
        os.system("""echo "%s 0" >> onebit.log """%obj);
