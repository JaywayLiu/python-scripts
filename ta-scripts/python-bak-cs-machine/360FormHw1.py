import csv
import os
import time

def MakeDict(a):
    if(not os.path.exists(a)):
        os.makedirs(a);

hwNo = 1
classNumber = "360"
emailPath = classNumber+"Email"
location="/home/jianwel/projects/360/hw/cpsc360/assignments/"

hwString = location+"hw"+str(hwNo)+"/"
MakeDict(emailPath);

with open('hw'+str(hwNo)+'-score-'+classNumber+'.csv', 'r') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    #print spamreader[1]
    title = reader.next();
    #print title[2];
    #titleList = title[1,-1];
    os.chdir(emailPath);
    MakeDict("hw"+str(hwNo));
    os.chdir("hw"+str(hwNo));
    i =0;
    while(1):
        l = reader.next();
        if i>0:
            break;
        i = i+1
        #print l
        emailAdd = l[0]+"@clemson.edu";
        emailFileName = l[0]+"_score.txt";
        ef = open(emailFileName, "w");
        ef.write("Hi "+l[0]+",\n\n")
        ef.write("Your score of lab"+ str(hwNo)+ " is "+l[1]+".\n\n")

        ef.write("The breakdown of score:\n");
        k =0;
        scoreList =l[2:-1];
        titleList =title[2:-1];
        #print titleList
        #for (s) in (scoreList):
        for (t) in (titleList):
            #jt = title[k];
            s = scoreList[k];
            if "comments" in t:
                ef.write("\n")
            ef.write(t + ": "+ s +"\n");
            k=k+1;

        ef.write("\n");
        ef.write("Final Score:" + l[1]);
        ef.write("\n\n");
        ef.write("************************************************************************\n");
        ef.write("Log of Compiling your files with our test files\n\n");

        ef.close();
        os.system("cat "+hwString+l[0]+"/make.log >> "+emailFileName);

        ef = open(emailFileName, "a");
        ef.write("\n\n");
        ef.write("************************************************************************\n");
        ef.write("Log of running your files with our test files\n\n");
        ef.close();

        os.system("cat "+hwString+l[0]+"/run.log >> "+emailFileName);

        #emailAdd = "jianwel@clemson.edu"
        emailAdd = "ljw725@gmail.com"
       # os.system('mail -s "'+ classNumber+" score - lab"+str(hwNo)+'" '+emailAdd + " < "+emailFileName);
        print('mail -s "'+ classNumber+" score - lab"+str(hwNo)+'" '+emailAdd + " < "+emailFileName);
        #time.sleep(1);
        #print emailAdd;
        #print l;
    #for row in spamreader:
    #   print ', '.join(row)
